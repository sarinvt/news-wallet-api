const responseMessage = {
  success: 'success',
  error: 'error',
  warning: 'warning'
}

export default Object.freeze(responseMessage);
