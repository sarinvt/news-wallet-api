export default class ResMessage {

  private message: String = null;
  private status: String = null;
  private code: Number = null;
  private data: Object = {};

  constructor(message: String, status: String, code: Number, data: Object) {
    this.message = message;
    this.status = status;
    this.code = code;
    this.data = data;
  }
}
