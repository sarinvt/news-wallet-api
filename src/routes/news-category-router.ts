import express from 'express';
import Message from '../app-models/res-message';
import resStatus from '../app-models/res-status';

import newsCategoryTestData from  '../test-data/news-categories-test-data';

let newsCategoryRouter = express.Router();

newsCategoryRouter.get('/', (req, res) => {
  res.status(200).json(new Message('', resStatus.success, 200, newsCategoryTestData));
});

newsCategoryRouter.get('/:id', (req, res)=>{
  res.status(200).json(new Message('', resStatus.success, 200, {}));
});

export default newsCategoryRouter;