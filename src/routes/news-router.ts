import express from 'express';
import Message from '../app-models/res-message';
import resStatus from '../app-models/res-status';

import newsTestData from  '../test-data/news-test-data';

let newsRouter = express.Router();

newsRouter.get('/', (req, res) => {
  res.status(200).json(new Message('', resStatus.success, 200, newsTestData));
});

newsRouter.get('/:id', (req, res)=>{
  res.status(200).json(new Message('', resStatus.success, 200, {}));
});

export default newsRouter;