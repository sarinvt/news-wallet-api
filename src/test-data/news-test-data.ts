import News from '../data-models/news';
import { format } from 'date-fns';

export default [
  new News('Test News',
    '1',
    'Sample Description',
    'https://ww.k.com/image.jpg',
    'The Hindu',
    'https://ww.k.com/image.jpg',
    'https://ww.k.com/image.jpg',
    'https://ww.k.com/image.jpg',
    'Author',
    format(new Date(), 'MM/DD/YYYY HH:mm:ss'))
];