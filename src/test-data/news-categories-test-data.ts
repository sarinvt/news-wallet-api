import NewsCategoy from '../data-models/news-category';

export default [
  new NewsCategoy('TEXT ONLY'),
  new NewsCategoy('IMAGE ONLY'),
  new NewsCategoy('IMAGE WITH TEXT'),
  new NewsCategoy('VIDEO WITH TEXT')
];