import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import Message from './app-models/res-message';
import sysConfig from './sys-config';
import responseStatus from './app-models/res-status';

import newsRouter from './routes/news-router';
import newsCategoryRouter from './routes/news-category-router';

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.status(200).json(new Message(`News-Wallet REST-API`, responseStatus.success, 200, {}));
});

app.use('/news', newsRouter);
app.use('/newsCategory', newsCategoryRouter);

app.listen(sysConfig.dev.port, () =>
  console.log(`News-Wallet-Api is running on port ${sysConfig.dev.port}`));