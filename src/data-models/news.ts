import getSysId from '../sys-factories/sys-id-generator';

export default class News {
  private newsId: String = null;
  private newsTitle: String = null;
  private newsType: String = null;
  private newsDescrption: String = null;
  private publisherLogoUrl: String = null;
  private publisher: String = null;
  private newsUrl: String = null;
  private imageUrl: String = null;
  private videoUrl: String = null;
  private author: String = null;
  private postedOn: String = null;

  constructor(
    newsTitle: String,
    newsType: String,
    newsDescrption: String,
    publisherLogoUrl: String,
    publisher: String,
    newsUrl: String,
    imageUrl: String,
    videoUrl: String,
    author: String,
    postedOn: String) {

    this.newsId = getSysId();
    this.newsTitle = newsTitle;
    this.newsType = newsType;
    this.newsDescrption = newsDescrption;
    this.publisherLogoUrl = publisherLogoUrl;
    this.publisher = publisher;
    this.newsUrl = newsUrl;
    this.imageUrl = imageUrl;
    this.videoUrl = videoUrl;
    this.author = author;
    this.postedOn = postedOn;
  }
}
