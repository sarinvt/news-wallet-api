export default class User {
    private name: String = null;
    private role: String = null;

    constructor(name: String, role: String) {
        this.name = name;
        this.role = role;
    } 
}