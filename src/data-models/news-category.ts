import getSysId from '../sys-factories/sys-id-generator';

export default class NewsCategory {
  private newsCategoryId: String = null;
  private newsCategory: String = null;

  constructor(newsCategory: String) {
    this.newsCategoryId = getSysId();
    this.newsCategory = newsCategory;
  }
}
