import {v4 as uuid} from 'uuid';

export default function getSysId() {
    return uuid();
}