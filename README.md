# news-wallet-api

Backend rest api for news-wallet application

### setting up project in local

* Follow this article to install node and yarn in your machine - https://yarnpkg.com/lang/en/docs/install/
    - It will auto detect the OS platform and give appropriate guidelines.
    - NOTE - Project have no dependency on npm. NEVER install yarn using npm.
    - Make sure you have latest versions of node and yarn (atleast node 8+ and yarn 1.9+) - node -v, yarn -v
* VS Code is Open Source / Easy to use / Recommended IDE.
* Use git to clone this project
    - git clone https://sarinvt@bitbucket.org/sarinvt/news-wallet-api.git
* `cd` into the project folder and run command `yarn`
* run command - `yarn build` for building the project.
* run command - `yarn dev` to serve the project in `localhost:3000`

### contribution guidelines
